package parser

type Obj struct {
	Prefix *Map     `@@?`
	Root   *ObjRoot `@@`
	Suffix []Map    `@@`
}

type ObjRoot struct {
	Block *Block `@@`
	Text  *Text  `|@@`
	Map   *Map   `|@@`
	Ref   *Ref   `|@@`
}

type RawBlock struct {
	Objs []Obj `@@`
}

type Block struct {
	OBrack string `@BlockOBrack`
	Objs   []Obj  `@@*`
	CBrack string `@BlockCBrack`
}

type Text struct {
	OSQuote string `@TextOSQuote`
	Text    string `@TextIn`
	CSQuote string `@TextCSQuote`
}

type Map struct {
	OParen   string    `@MapOParen`
	RawBlock *RawBlock `@@`
	CParen   string    `@MapCParen`
}

type Ref struct {
	Ident string `(@RefNonDot*@RefDot)+`
}
